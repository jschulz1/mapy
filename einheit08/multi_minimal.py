# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 17:08:54 2014

@author: jschulz1
"""

from multiprocessing import Pool
from pylab import *              # Matplotlib's pylab interface
import time
from functools import partial

def f(x,a):
    return sin(x) + x + exp(x)*a

p = Pool(2)
li = randn(100000,1)


pf = partial(f, a=5)

print ("Pool map")
start = time.time()
res = p.map(pf, li) 
end = time.time()
secs = end - start
print ("solution after {} s".format(secs))

print ("normal sequential map")
start = time.time()
res = map(lambda x: sin(x) + x + exp(x), li) 
end = time.time()
secs = end - start
print ("solution after {} s".format(secs))

print ("normal loop")
start = time.time()
res = []
for el in li:
    res.append(sin(el) + el + exp(el))
end = time.time()
secs = end - start
print ("solution after {} s".format(secs))


