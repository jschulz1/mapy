# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 15:01:26 2014

@author: jschulz1
"""
from pylab import *              # Matplotlib's pylab interface
import time

a = randn(1000000)
d = zeros(a.shape[0]-1)
start = time.time()
for i in range(0,len(d)):
    d[i] = a[i]-a[i+1]
end = time.time()
secs = end - start
print ("loop needed {} s".format(secs))

start = time.time()
d[:] = a[:-1]-a[1:]
end = time.time()
secss = end - start
print ("numpy slicing needed {} s".format(secss))
print ("slices are {} times faster".format(secs/secss))



a = [randn(3) for i in range(100000)]


d = zeros(len(a),)

start = time.time()
for i in range(len(a)):
    d[i] = norm(a[0] - a[i])
end = time.time()
secs = end - start
print ("loop needed {} s".format(secs))


start = time.time()
d = map (norm,a[0] - a[:])
list(d)
end = time.time()
secs = end - start
print ("map/slice needed {} s".format(secs))

    
    