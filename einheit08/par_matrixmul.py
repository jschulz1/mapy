# -*- coding: utf-8 -*-
"""
Created on Sun Feb  2 21:35:46 2014

@author: jschulz1
"""

from mpi4py import MPI
import numpy as np
from pylab import *              # Matplotlib's pylab interface
import time

comm = MPI.COMM_WORLD
size = comm.Get_size() # number of threads
rank = comm.Get_rank() # id of thread

n = 1000

if rank == 0:
    A = [] 
    x = ones(n)
    for idx in range(1,size+1):
        A.append( array([[i*j for j in  range(1,n+1)] for i in range (idx*n,idx*n+n/size+1) ]))
    #print A
    Amat = vstack(A)
    #print Amat
    start = time.time()
    xref = dot(Amat,x)
    end = time.time()
    secsr = end - start
    print ("refsolution after {} s".format(secsr))
else:
    A = None
    x = None

start = time.time()

# communication    
A = comm.scatter(A, root=0) # scatter A
x = comm.bcast(x, root=0) # broadcast x
#print "rank {} and matrix {}".format(rank,A.shape)
#assert A == (rank+1)**2

# do matrix-multiply for every thread
b = np.dot(A, x)

#print "rank {} before gathering {}".format(rank,b)

# get the results from all threads
b = comm.gather(b, root=0)

if rank == 0:
    bvec = hstack(b)
    end = time.time()
    secs = end - start
    print ("solution after {} s".format(secs))
    print ("norm of diff {}".format(norm(xref-bvec)))
    print ("multithreaded factor {} faster".format(secsr/secs))


