# -*- coding: utf-8 -*-
"""
Created on Mon Aug 12 17:57:31 2013

@author: jschulz1
"""

import numpy as np  # NumPy (multidimensional arrays, linear algebra, ...)
import scipy as sp  # SciPy (signal and image processing library)
import scipy.sparse as sparse # sparse-matrices
import scipy.sparse.linalg as linalg # linalg for sparse

import matplotlib as mpl         # Matplotlib (2D/3D plotting library)
import matplotlib.pyplot as plt  # Matplotlib's pyplot: MATLAB-like syntax
from mpl_toolkits.mplot3d import Axes3D # simple matlab-like plot-lib
from mayavi import mlab as ml #majavi mlab
from pylab import *              # Matplotlib's pylab interface

#@profile
def poisson(N,h):
    """ Poisson matrix 2D"""
    # Second-Derivative Matrix
    data = np.ones((3, N))
    data[1,] = -2*data[1,]
    diags = [-1,0,1] 
    D2 = sparse.spdiags(data,diags,N,N)
    locator = sparse.eye(N,N)
    A = (1/h**2)*(sparse.kron(locator,D2) + sparse.kron(D2,locator))    
    print A.todense()
    return A
 

def simpleboundary(x):
    res = np.zeros(size(x,1))
    for i in range(size(x,1)):
        if x[0][i] <= 0:
            res[i] = 0
    return res
        
def boundary_conditions(r,N,h):
    """ create vector with boundary conditions
        u(x,0) = R on \delta \Omega"""
    b = np.zeros(N**2)
    x0 = np.zeros(N)
    x1 = np.ones(N)
    x = np.linspace(0,1,N)
    b[0::N] = -r([x,x1]) #boundary 1 top
    b[0:N:1] += -r([x0,x]) # boundary 2 left
    b[N-1::N] += -r([x,x0]) # boundary 3 bottom
    b[(N-1)*N::1] += -r([x1,x]) #boundary 4 right

    return 1/h**2*b


def initial_conditions(x,y):
    """create vector with Initial conditions
        u(x,0) = F"""
    u = np.zeros((N,N))
    for i in range(N):
        for j in range(N):
            if ( ( (x[i]-0.5)**2+(x[j]-0.5)**2 <= 0.1)
                & ((x[i]-0.5)**2+(x[j]-0.5)**2>=.05) ):
                    u[i,j] = 1
    u = u.reshape(N**2)  #reshaping vector into form for calculation
    return u

def CreateMovie(path, fname, fps=10):
    """ creates movie with ffmpeg"""
    import os
 
    os.system("rm "+path+"/"+fname+".mp4")
    os.system("ffmpeg -framerate "+str(fps)+" -i "+path+"/_tmp%05d.png "+path+"/"+fname+".mp4")
    #os.system("rm /tmp/_tmp*.png")
    

a = 0.5 # Diffusion constant.
N = 100 # number of points
saveflag = False
Nt = 100 #number of time steps

# Define the Frame Speed and Movie Length
movieflag = False
FPS = 20
MovieLength = 10

# create grid
x,h = np.linspace(0,1,N+2,retstep=True)
y = np.linspace(0,1,N+2)
X,Y = sp.meshgrid(x,y)
  
  
print ("creating system matrix")
A = poisson(N,h) 
print ("setting boundary conditions")
b = boundary_conditions(simpleboundary,N,h)

    
# For stability: calculate time-step:
ht = h**2*h**2/( 2*a*(h**2+h**2) )
# Data for each time-step
data = []
print ("setting initial conditions")
u = initial_conditions(x,y) 
data.append(u) 
#initialize plot
fig = plt.figure(2)
img = subplot(111)
im = img.imshow( data[0].reshape(N,N),cmap=cm.hot, interpolation='nearest', origin='lower')
fig.colorbar(im)
manager = get_current_fig_manager() #handles to the figure
    
mlf = ml.figure(bgcolor=(1,1,1))
mls = ml.surf(X.T,Y.T,data[0].reshape(N,N)) 
ml.axes()
ml.colorbar()

# explicit Euler method for time evolution
print ("**** beginning explicit euler")
for t in range(Nt):
    print ("** timestep {}".format(t))
    # explicit euler
    data.append( data[t] + ht*a*(A*data[t] + b)) 

    #update image
    im.set_array(data[t].reshape(N,N))
    manager.canvas.draw()
    # mayavi
    mls.mlab_source.scalars = data[t].reshape(N,N)
    ml.draw() # redraw seems not to work. only works non-interactively
        
    if saveflag:    
        fname = '/scratch/jschulz1/heat/_tmp{:05d}.png'.format(t) 
        #fig.savefig(fname)
        ml.savefig(fname)

# Generate the movie
if movieflag:
    print ("creating movie")
    CreateMovie('/scratch/jschulz1/heat','heatmovie', FPS)

