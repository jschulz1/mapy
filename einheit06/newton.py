# -*- coding: utf-8 -*-
"""
Created on Sun Sep 15 23:23:19 2013

@author: jschulz1
"""

from mpl_toolkits.mplot3d import Axes3D
from pylab import *              # Matplotlib's pylab interface


def newton_example():
    # Programm zur Loesung von x=cos(x)
    xr = 0.739085133215161 # wahre Loesung
    anzit = 15
    xn = zeros(anzit+1)
    xn[0] = 1# Startwert Newton
    for i in range(0,anzit):
        xn[i+1] = xn[i]-(xn[i]-cos(xn[i]))/(1+sin(xn[i])) # Newton
    xnlist = abs(xn-xr) # Fehlerliste 
    return (anzit,xnlist)


def newton_konvergenz():
    [X,Y] = mgrid[-1:1:1000j,-1:1:1000j]
    Z = X + 1j*Y
    TOL = 0.02
    V = zeros(shape(X))
    anzit = zeros(shape(X))
    for idx in range(0,20):
        Z = Z - (Z**3-1)/(3*Z**2)
        ind = find (Z**3 - 1 < TOL)
        V.ravel()[ind] = angle(Z.ravel()[ind])
        anzit.ravel()[find (Z**3 - 1 > TOL)] = idx
    return (V,anzit)

(anzit,xnlist) = newton_example()
semilogy(range(0,anzit+1),xnlist,'r*-')
title('Loesung von $x=cos(x)$')
xlabel('Iterationen')
ylabel('Fehler im $x$-Wert')



(V,anzit) = newton_konvergenz()

figure()
imshow(V)
colorbar()
title('Konvergenzverhalten Newton - Loesung')
xlabel('$x$'),ylabel('$i*y$')

figure()
imshow(anzit)
colorbar()
title('Konvergenzverhalten Newton - Anzahl iterationen')
xlabel('$x$'),ylabel('$i*y$')


show()
print (roots([1, 0, 0, -1]))