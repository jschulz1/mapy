# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 19:26:55 2013

@author: jschulz1
"""

import scipy
from pylab import *              # Matplotlib's pylab interface

from traits.api import HasTraits, Instance, on_trait_change, Int, Str, Float, Tuple #Trait-types
from traitsui.api import View, Item, Group # traitsUI 
from chaco.api import ArrayPlotData, Plot
from enable.component_editor import ComponentEditor

def integral2(f='x**3',N=20,a=(0.,1.)):
    """berechnet approximativ ein Integral 
    ueber  (0,1) durch die Mittelpunktregel"""
    h = (a[1]-a[0])/N
    x = linspace(a[0]+h/2,a[1]-h/2,N)  

    g = scipy.__dict__
    y = eval(f,g,{'x': x})
    #print (y)
    #print (type(y))
    # Berechnung des Integrals
    return (a[1]-a[0])*sum(y)*(1./N)

    #for arg in args:
    #    t[0]anzahl_parameter = len(args)
        
    #for name, value in kwargs.items():
    #    print '{0} = {1}'.format(name, value)



#creating class inheriting from the class HasTraits
class Integral(HasTraits):
    N = Int (20)
    a = Float(0)
    b = Float(1)
    function = Str('x**3', desc='function', auto_set=False, enter_set=True)

    #result = Property(depends_on='N,a,b,function')
    result = Float(0)
    #result = integral2(f,N,(a,b))

    plot = Instance(Plot)
    # Plot
    #x1 = linspace (a,b,N+1)
    #figure()
    #for i in range(0,N):
    #    fill_between([ x1[i],x1[i+1]], [y[i],y[i]] ,facecolor='r')
    #xplot = arange(a[0],a[1],(a[1]-a[0])/100)
    #plot(xplot,f(xplot),linewidth=3)
    #title('$\int_{{{}}}^{{{}}} {}$ = {} fuer N = {}'.format(a[0],a[1],fstr,result,N),fontsize=20)

    # initialization
    def __init__(self):
        # Do not forget to call the parent's __init__
        HasTraits.__init__(self)
        #super(Integral, self).__init__(*args, **kw)
        x1 = linspace (self.a,self.b,self.N+1)
        y1 = x1**3
        plotdata = ArrayPlotData(x=x1,y=y1)
        plot = Plot(plotdata)
        plot.plot(("x","y"), type = "line", color = "blue")
        self.plot = plot
        
    # when the scene is activated change the camera viewpoint    
    #@on_trait_change('scene.activated')
    #def create_plot(self):
    #    self.scene.mlab.view(45,210)

    # when the user changes one of the 2 traits in the GUI, redraw the image.
    @on_trait_change('a,b,N,function')
    def update_plot(self):
        x1 = linspace (self.a,self.b,self.N+1)
        g = scipy.__dict__
        y1 = eval(self.function,g,{'x': x1})
        plotdata = ArrayPlotData(x=x1,y=y1)
        plot = Plot(plotdata)
        plot.plot(("x","y"), type = "line", color = "blue")
        self.plot = plot
        self.result = integral2(self.function,self.N,(self.a,self.b))
        self.plot.remove() # remove plot (to be able to recreate it)
        #self.plot = self.scene.mlab.surf(self.x[:,:,0],self.y[:,:,0],self.Z[:,:,self.time-1])

    # the layout of the dialog created
    view = View(Group( 'N','a','b', 'function'
                    ,orientation='horizontal',layout='normal'),
                    Item('plot', editor = ComponentEditor(), show_label = False),
                    Group('result',orientation='horizontal',layout='normal'),
                kind='live', title='integral GUI', width = 500, height = 500, resizable = True
                )

# creating new object of the class
integral = Integral()
# starting GUI
integral.configure_traits()

        
#integral(N=20,a=(0.,1.))
#integral(a=(0.,1.),N=20)
#integral(20,(0.,1.))

#integral2(lambda x: log(x**2),20,(1.,5.),fstr='\log(x^2)')