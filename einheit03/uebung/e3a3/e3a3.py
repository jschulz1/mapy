# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 13:49:11 2013

@author: jschulz1
"""

from pylab import *              # Matplotlib's pylab interface

a = 5
x = ogrid[-3:3:40j]
f = 1./(x**2+a)

figure()
plot (x,f,linewidth=2)
grid('on')
box('on')
title('$f(x) =\\frac{1}{x^2+a}$')
show()
