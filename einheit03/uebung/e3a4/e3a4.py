# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 13:53:47 2013

@author: jschulz1
"""

from pylab import *              # Matplotlib's pylab interface

x = ogrid[0:1:100j]

figure()
xlabel('x')
ylabel('y')
grid ('on')
box ('on')
xlim ([-0.1, 1.1])
ylim ([-1.1, 1.1])
plot (x,sin(0.5*pi*x),'b--',linewidth=2)
plot (x,sin(pi*x),'k:',linewidth=2)
plot (x,sin(2*pi*x),'g-',linewidth=2)
plot (x,sin(4*pi*x),'m-.',linewidth=2)
legend (('$\sin(0.5 \pi x)$','$\sin(\pi x)$','$\sin(2 \pi x)$','$\sin(4 \pi x)$'),loc='lower left')
show()