# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 14:23:27 2013

@author: jschulz1
"""

from mpl_toolkits.mplot3d import Axes3D # simple matlab-like plot-lib
from pylab import *              # Matplotlib's pylab interface

[X,Y] = mgrid[-1:1:40j,-1:1:40j]

# Benutze dictionary fuer die optionen an surf
# diese koennen "ausgepackt" werden mit dem **-operator
# **options ist dasselbe als folgende Argumente zu setzen:
# rstride=1,cstride=1,cmap=cm.jet,linewidth=0
options = {'rstride':1,'cstride':1,'cmap':cm.jet,'linewidth':0}

fig = figure()
ax = fig.add_subplot(2, 2, 1, projection='3d')
ax.plot_surface (X,Y,sin(pi**2*X*Y), **options)

ax = fig.add_subplot(2, 2, 2, projection='3d')
ax.plot_surface (X,Y,(X**2 -1)*(Y**2 -1),**options)

ax = fig.add_subplot(2, 2, 3, projection='3d')
ax.plot_surface (X,Y,sin(pi*X**2), **options)

ax = fig.add_subplot(2, 2, 4, projection='3d')
ax.plot_surface (X,Y,sin(-pi*exp(-X**2 - Y**2) ),**options)
show()