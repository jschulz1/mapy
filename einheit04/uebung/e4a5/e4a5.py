# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 17:33:40 2013

@author: jschulz1
"""
from pylab import *              # Matplotlib's pylab interface

def potenz (x,n):
    """ Berechnet rekursiv die natuerliche Potenz einer Zahl   
    input: 
        x:  Zahl x
        n:  Potenz
    output:
        result:  Ergebnis"""
    if n == 1:
        return x
    if mod(n,2) == 0:
        return potenz(x,n/2)*potenz(x,n/2)
    else:
        return x*potenz(x,(n-1)/2)*potenz(x,(n-1)/2)

x = 4
n = 3
print (potenz (x,n))
                 