# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 14:44:40 2013

@author: jschulz1
"""

from pylab import *              # Matplotlib's pylab interface
import re

# Daten lesen (x, f(x) ignorieren)
daten = genfromtxt('daten.dat', dtype=[float,float])

# das x, f(x) am Ende macht alles etwas komplizierter wenn wir das auch
# auslesen wollen. Mit den Mitteln die wir kennen ist es am einfachsten
# einfach nochmal per regex nach dem x und f(x) zu suchen
# Sonstige Textinformationen aus der Datei lesen

fid = open('daten.dat','r')
alles = fid.read()
fid.close()
daten = re.findall('([\d.-]+)\s+([\d.-]+)',alles)
daten = array(daten,dtype=float)
lz = re.search('([a-z()]+)\s+([a-z()]+)',alles)

x = daten[:,0]
f = daten[:,1]
figure()
plot (x,f)
xlabel(lz.group(1))
ylabel(lz.group(2))