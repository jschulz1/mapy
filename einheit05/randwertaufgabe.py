# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 00:23:11 2013

@author: jschulz1
"""

from pylab import *              # Matplotlib's pylab interface

def randwertaufgabe(n):
    """berechnet mit Finiten Differenzen die Lösung u von
      -u''=f in (0,1), u(0)=u(1)=0
      n =  Anzahl Stuetzstellen
      @author Gerd Rapin, Jochen Schulz """

    # Erzeugen des Gitters
    x = linspace(0,1,n)
    x_i = x[1:n-1]
    # Aufstellen des lin. Gls.
    #A = diag(2*ones(n-2),0)+diag(-1*ones(n-3),-1)+diag(-1*ones(n-3),1)
    Ab = diag(2*ones(n),0)+diag(-1*ones(n-1),-1)+diag(-1*ones(n-1),1)
    Ab[0:2,0:2] = [[1,0],[-1,2]]
    Ab[-2:,-2:] = [[2,-1],[0,1]]
    F = (1./n)**2*exp(x_i) # rechte Seite fuer f=exp(x)
    Fb = hstack([0, F , 0 ])
    # Loesen des lin. Gls.
    z = solve(Ab,Fb)
    return (x,z)


(x,z) = randwertaufgabe(41)
# Darstellen der Loesung
plot(x,z,'r*-');
show()
#plot(x_i,exp(x_i))
