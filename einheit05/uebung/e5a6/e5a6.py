
from pylab import *              # Matplotlib's pylab interface

# Anzahl Stützstellen
n = 101

#% Erzeugen des Gitters
x = arange(0,1,1/n)
x_i = x[1:n-1]

# Aufstellen des lin. Gls.
A = diag(2*ones(n-2),0)+diag(-1*ones(n-3),-1)+diag(-1*ones(n-3),1)
F = (1/n)**2*ones(n-2) # rechte Seite für f=1 

# Lösen des lin. Gls.
z_i = solve(A,F)

# Darstellen der Lösung
z = hstack((0, z_i, 0))
figure()
plot(x,z,'r*-')