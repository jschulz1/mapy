from pylab import *              # Matplotlib's pylab interface

A = array([[30, 1, 2, 3], [4, 15, -4, -2], [-1, 0, 3, 5], [-3, 5, 0, -1]])
[V,D] = eig(A)
[Q,R] = qr(A) 
print (V,D)
print (Q,R)