# -*- coding: utf-8 -*-
"""
Created on Wed Sep 11 11:20:56 2013

@author: jschulz1
"""

from pylab import *              # Matplotlib's pylab interface
from poly import ausw_poly2

def interpol2(x,y):
    #Aufstellen des lin. GLS
    A = vander(x)
    #Loesen des lin GLS
    p = solve(A,y)
    return p

def interpolation(f1,x):
    assert callable(f1), "Bitte eine Funktion übergeben"

    #Stuetzstellen
    y = f1(x)
    #Berechnen der Koeffizienten
    p = interpol2(x,y)

    #Plotten
    figure()
    plot(x,y,'or',markersize=8)
    x1 = linspace(-5,5,100)
    y1 = ausw_poly2(p[::-1],x1)
    y2 = f1(x1)
    plot(x1,y1,x1,y2,linewidth=3)
    xlim([-6,6])
    grid('on')
    box('on')
    legend(('Interpolationspunkte','Interpolierende von f','$f(x)=1/(1+x^2)$'),loc='best')
    return p[::-1]

