# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 16:26:12 2015

@author: jschulz1
"""
from mpl_toolkits.mplot3d import Axes3D
from pylab import *              # Matplotlib's pylab interface



def f (fun,N=50):
    # auswerten
    x = linspace (-1,1,N)
    [X,Y] = meshgrid(x,x)
    values = fun(X,Y)

    #plot
    fig = figure(figsize=(16,12))
    ax = Axes3D(fig)
    ax.plot_surface(X,Y,values,rstride=1,cstride=1,cmap=cm.jet,linewidth=0)
    
f (lambda x,y: x**2+y)
f (lambda x,y: x**2+y,100)