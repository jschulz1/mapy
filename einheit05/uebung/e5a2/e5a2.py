# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 16:56:07 2015

@author: jschulz1
"""

from pylab import *              # Matplotlib's pylab interface
from integral2 import integral2

integral2(lambda x: x**3,20,(-3.,3.),fstr='x^3')
integral2(lambda x: x**2*sin(pi*x),20,(1.,4.),fstr='x^2 \sin(\pi x)')
