# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 17:01:44 2015

@author: jschulz1
"""

from pylab import *              # Matplotlib's pylab interface

x1 = array([0, 1, 0, 1])
x2 = array([1, 2, 3, 4])
x3 = array ([1, 0, 1, 0])
x4 = array ([0, 0, 1, 1])
A = array([x1, x2, x3, x4])
# det A = 0 bedeutet linear unabhaengig
print( det(A))
# eig A enthaelt Eigenwert 0
print (eig(A))
# Loesung der Gleichung. Bei Nullvektor = linear unabhaengig
# Hier koennen auch die Abhaengigkeiten untereinander abgelesen werden.
b = [0, 0, 0, 0]
x = solve(A,b)
