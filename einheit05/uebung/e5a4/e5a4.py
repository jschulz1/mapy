# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 17:08:05 2015

@author: jschulz1
"""

from pylab import *              # Matplotlib's pylab interface

def randwertaufgabe(n):
    """berechnet mit Finiten Differenzen die Lösung u von
      -u''=f in (0,1), u(0)=u(1)=0
      n =  Anzahl Stuetzstellen      
      @author Gerd Rapin, Jochen Schulz """
    if (n< 20 or n > 200): 
        print ("Stuetzstellen nicht im Bereich von 20 bis 200")
        return  

    # Erzeugen des Gitters
    x = linspace(0,1,n)
    x_i = x[1:n-1]
    # Aufstellen des lin. Gls.
    A = diag(2*ones(n-2),0)+diag(-1*ones(n-3),-1)+diag(-1*ones(n-3),1)
    F = (1./n)**2*exp(x_i) # rechte Seite fuer f=exp(x) 

    # Loesen des lin. Gls.
    z_i = solve(A,F)
    return z_i


z_i = randwertaufgabe(41)
# Darstellen der Loesung
z = hstack((0, z_i, 0))
plot(z,'r*-');
show()
#plot(x_i,exp(x_i))
